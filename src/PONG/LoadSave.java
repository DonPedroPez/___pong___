/*
 * ---^___PONG___^---
 */

package PONG;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Rybasso
 */
public class LoadSave {
    ArrayList <String> players = new ArrayList<String>();
    ArrayList <String> winners = new ArrayList<String>();
    
    public void add(String x) {
        winners.add(x);
        Boolean exist = false;
        for(String i : players) {
            if(x.equals(i)) exist = true;
        }
        if(!exist) players.add(x);
    }
    
    public ArrayList<String> getPlayers() {
        return players;
    }
    
    public ArrayList<String> getWinners() {
        return winners;
    }
    
    public void save() throws IOException {
        ObjectOutputStream play = null;
        try {
            play = new ObjectOutputStream(new FileOutputStream("players.ser"));
            play.writeObject(players);
        } finally {
            if(play!=null) play.close();
        }
        ObjectOutputStream win = null;
        try {
            win = new ObjectOutputStream(new FileOutputStream("winners.ser"));
            win.writeObject(winners);
        } finally {
            if(win!=null) win.close();
        }
    }
    
    public void load(String x) throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream play = null;
        try {
            play = new ObjectInputStream(new FileInputStream("players.ser"));
            players=(ArrayList<String>) play.readObject();
        } catch (FileNotFoundException ex) {
            players.add(x);
        } finally {
            if(play!=null) play.close();
        }
        ObjectInputStream win = null;
        try {
            win = new ObjectInputStream(new FileInputStream("winners.ser"));
            winners=(ArrayList<String>) win.readObject();
        } finally {
            if(win!=null) win.close();
        }
    }
}
