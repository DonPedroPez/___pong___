package PONG;

import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JOptionPane;

/*
 * ---^___PONG___^---
 */

/**
 *
 * @author Rybasso
 */
public class PONG extends JFrame {

    public PONG(String player)
    {
        add(new Board(player));
        setTitle("---^___PONG___^---");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(Commons.WIDTH, Commons.HEIGTH);
        setLocationRelativeTo(null);
        setIgnoreRepaint(true);
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        String player = JOptionPane.showInputDialog(null, "What's your name?: ", null);
        String mess = "Well... You're going to play anyway, NN!";
        if(player != null) {
            mess = "Hello " + player + "! Let's play PONG!";
        } else {
            player = "NN";
            mess = "Well... You're going to play anyway, NN!";
        }
        if(player.equals("")) {
            mess = "Well... You're going to play anyway, NN!";
            player = "NN";
        }
        JOptionPane.showMessageDialog(null, mess);
        new PONG(player);
    }
    
}

